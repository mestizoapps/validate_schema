//Librerias
var express = require("express");
var normalise = require("ajv-error-messages")
var Ajv = require("ajv");
var fs = require("fs");
var bodyParser = require('body-parser');

var port = process.env.PORT || 3000;
var app = express();
var ajv = new Ajv({allErrors: true});
var localize =  require("ajv-i18n");

app.use("/public",express.static("public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var modulos = ["departamento","sucursal"];


	//Endpoint para post
	app.post('/', function(getData, resData){
		resData.writeHead(200,{"content-type":"application/json"});
		
		var schema; 
			
		try{
			eval(schema = JSON.parse(fs.readFileSync('schemas/departamento.json')));
		}catch(e){
			console.log("errors");
		}
		schema.required = JSON.parse(fs.readFileSync('schemas/post_departamento.json'));

		var data = getData.body;
		var validate = ajv.compile(schema)
		var valid = validate(data);
		var descripcion = descripcion || [];
				
		if(valid == true){	
			descripcion = "Datos correctos";
			resData.end(JSON.stringify({success:valid,description:descripcion}));

		}else{
			localize.es(validate.errors);
			validate.errors.forEach(elem=>{
				var code = elem.dataPath.slice(1);
				descripcion.push({code : code, message : elem.message});
			});
			
			console.log(descripcion);
			resData.end(JSON.stringify({success:valid, errors:descripcion}));
		}
		console.log(valid, descripcion);
		
	});

//Se inicializa el endpoint
app.listen(port);
