//Librerias
var express = require("express");
var normalise = require("ajv-error-messages")
var Ajv = require("ajv");
var fs = require("fs");
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;
var app = express();
var ajv = new Ajv({allErrors: true});
var localize =  require("ajv-i18n");
//var localize_es = require('ajv-i18n/localize/es');

app.use("/public",express.static("public"));
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: true }));

var modulos = ["departamento","sucursal","usuario"];
modulos.forEach(elem =>{

	//Endpoint para post
	app.post('/'+elem, function(getData, resData){
		resData.writeHead(200,{"content-type":"application/json"});
		try{
			var schema = JSON.parse(fs.readFileSync('schemas/'+elem+'.json'));
			schema.required = JSON.parse(fs.readFileSync('schemas/post_'+elem+'.json'));
		}catch(e){
			console.log("JSON no valido");
		}
		var data = getData.body;
		var validate = ajv.compile(schema)
		var valid = validate(data);
		var descripcion = descripcion || [];
				
		if(valid == true){	
			descripcion = "Datos correctos";
			resData.end(JSON.stringify({success:valid,description:descripcion}));

		}else{
			localize.es(validate.errors);
			validate.errors.forEach(elem=>{
				var code = elem.dataPath.slice(1);
				descripcion.push({code : code, message : elem.message});
			});
			
			//console.log(descripcion);
			resData.end(JSON.stringify({success:valid, errors:descripcion}));
		}
		//console.log(data);
		var tm = new Date();
		console.log(tm, valid, descripcion);
	});

	//Endpoint para put
	app.put('/'+elem, function(getData, resData){
		resData.writeHead(200,{"content-type":"application/json"});
		var schema = JSON.parse(fs.readFileSync('schemas/'+elem+'.json'));
		schema.anyOf = JSON.parse(fs.readFileSync('schemas/put_'+elem+'.json'));;
		
		var data = getData.body;
		var valid = ajv.validate(schema, data);
		
		var data = getData.body;
		var validate = ajv.compile(schema)
		var valid = validate(data);
		var descripcion = descripcion || [];
				
		if(valid == true){	
			descripcion = "Datos correctos";
			resData.end(JSON.stringify({success:valid,description:descripcion}));

		}else{
			localize.es(validate.errors);
			validate.errors.forEach(elem=>{
				var code = elem.dataPath.slice(1);
				descripcion.push({code : code, message : elem.message});
			});
			
			//console.log(descripcion);
			resData.end(JSON.stringify({success:valid, errors:descripcion}));
		}
		//console.log(data);
		console.log(valid, descripcion);
	});
	console.log("Server http://192.168.1.138:"+port+'/'+elem);
});
//Se inicializa el endpoint
app.listen(port);
